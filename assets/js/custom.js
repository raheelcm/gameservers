$(document).ready(function() {
$('#slider-two').owlCarousel({
	items:3,
    rtl:true,
    loop:true,
    lazyLoad:true,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    slideBy: 1,
	margin:30,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})
$('#slider-plans').owlCarousel({
	items:3,
    rtl:true,
    loop:true,
    lazyLoad:true,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    slideBy: 1,
	margin:30,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})
});