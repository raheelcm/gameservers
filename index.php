<?php
session_start();
$pageName = (isset($_GET["page"]) ? $_GET["page"] : "home");
	if(!file_exists(__DIR__ . "/pages/" . $pageName . ".php")) {
		$pageName = "home";	
	}
?>
<!DOCTYPE html>
<html dir="rtl" lang="he">
<head>
	<link rel="icon" type="image/png" href="https://i.imgur.com/lwN5Zjf.png" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="חברת שרתי משחק מתקדמת, מחירים ומבצעים זולים אחסון שרתי משחק , אחסון אתרים CS1.6 CS:GO FiveM
        שרתי קונטר סטרייק, שרתי דיבור, שרת, שרת משחק, סרברים, סרברים לקונטר, סרברים לגלובל אופנסיב, סרברים למיינקראפט, סרברים לראסט, סרברים .">
    <meta name="author" content="< AMArt - Coder Professional />">
    <meta name="google-site-verification" content="jU1xPxZ6ARoGCrvQ-d01na1oka-1yrvUQsjs3Iu7ZLE" />
    <title>GameServers.co.il - שרתי משחק כחול לבן</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-rtl.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link href="assets/css/style.css?v=1.3" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5e88ce3b35bcbb0c9aadd2ce/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script--

<script src="//t1.extreme-dm.com/f.js" id="eXF-zomby111-0" async defer></script>

<!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-162557485-1"></script><script>  window.dataLayer = window.dataLayer || [];  function gtag(){dataLayer.push(arguments);}  gtag('js', new Date());  gtag('config', 'UA-162557485-1');</script>

	
	<style>
	#admin_menu { text-align: center; padding: 5px; margin-bottom: 10px; }
	.bordered { padding: 5px; border: 1px solid #1fc1f5; border-radius: 5px; font-size: 16px; }
	input.form-control {
		    display: block;
			width: 100%;
			height: 34px;
			padding: 6px 12px;
			font-size: 14px;
			line-height: 1.42857143;
			color: #555;
			background-color: #fff !important;
			background-image: none;
			border: 1px solid #ccc !important;
			border-radius: 4px !important;
			-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			-webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
			-o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
			transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	}
	#order_details { text-align: center; }
		@media screen and (max-width: 768px) {
			.boldgr {font-size: 48px !important; }	
			.colorvi { font-size: 26px !important; }
		}
	</style>
</head>
<body>

   <header>
        <div class="navbar-all">
		<nav class="navbar navbar-default">
			<div class="container header">
				<a class="pull-left logof" href="home"><img src="assets/images/logo.png" alt class="img-responsive pull-left"></a>
				<div class="navbar-header pull-right">
					<button type="button" class="navbar-toggle collapsed pull-right" id="toggle-navbar" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
				  <ul class="nav navbar-nav">
                    <li<?php if($pageName == "home") echo ' class="inpaged inpage"'; ?>><a href="home">דף ראשי</a></li>
					<li<?php if($pageName == "gameserver") echo ' class="inpaged inpage"'; ?>><a href="gameserver">שרתי משחק</a></li>
					
					<li<?php if($pageName == "takannon") echo ' class="inpaged inpage"'; ?>><a href="takannon">תקנון</a></li>
					<li<?php if($pageName == "contact") echo ' class="inpaged inpage"'; ?>><a href="contact">צור קשר</a></li>
					<li><a href="https://hosting1.site/" target="_blank">פאנל ניהול</a></li>
				  </ul>
				</div>
			</div>
		</nav>
        </div>
	</header>
	<?php require(__DIR__ . "/pages/" . $pageName . ".php"); ?>
	<section class="footer">
	<div class="container">
		<div class="social-btns">
			<a class="btn facebook-f" href="https://www.facebook.com/GameServerscoil-109383750722232" target="_blank"><i class="fab fa-facebook-f"></i></a>
			<a class="btn discord" href="https://discordapp.com/invite/makNfrv" target="_blank"><i class="fab fa-discord"></i></a>
			<a class="btn youtube" href="-" target="_blank"><i class="fab fa-youtube"></i></a>
		</div>
		<a href="home" class="footer-logo"><img src="assets/images/logofooter.png"></a>
			<br>
		<h2>כל הזכויות שמורות בלבד ל <b>GameServers.co.il</b><span class="lefts"></a></span></h2>
    </div>
	</section>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/custom.js"></script>
</body>
</html>