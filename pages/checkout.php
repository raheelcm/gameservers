<?php
?>
    <div class="headerweb">
    </div>
    <section class="contact">
        <div class="container">
            <h2>תשלום</h2>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-xs-6">
                    <h2> הרשמה </h2>
                    <form method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="שם מלא" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="שם משתמש" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="user@email.com" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="******" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="passwordverify" placeholder="******" required>
                        </div>
                        <div class="form-group">
                            <input type="phone" class="form-control" name="פיםמק" placeholder="050-123-4567" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="discord" placeholder="לא חובה" required>
                        </div>
                        <input type="submit" name="submit" value="הירשם" style="margin: 0 auto;">
                    </form>
                </div>

                <div class="col-xs-6">
                    <h2> התחברות </h2>
                    <form method="post" name="login">
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="שם משתמש" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="*****" required>
                        </div>
                        <input type="submit" name="submit" value="התחבר" style="margin: 0 auto;">
                    </form>
                </div>

                <div class="col-xs-6">
                    <h2> פירוט הזמנה </h2>
                    <div id="order_details">
                        <p>סוג משחק: Counter Strike</p>
                        <p>כמות שחקנים: 32</p>
                        <p>כמות חודשיים: 3</p>
                        <p>פאנל ניהול: GameServers</p>
                        <p>מיקום: Israel</p>
                        <p>תמיכה: 24/7</p>
                        <p>הגנות: DDOS</p>
                        <p>מחיר: 80 ש"ח</p>
                        <a href="#movetopaypalpurchase" class="bordered">מעבר לתשלום</a>
                    </div>

                </div>

            </div>
        </div>
    </section>