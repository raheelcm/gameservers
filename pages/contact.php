<?php
  
  //session_start();

  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;
  use PHPMailer\PHPMailer\Exception;

  require_once('PHPMailer/Exception.php');
  require_once('PHPMailer/PHPMailer.php');
  require_once('PHPMailer/SMTP.php');

  function csrf_field() {
    //Create CSRF Filed in form csrf_field();
    if(!isset($_SESSION['token'])) {
    $token = hash('sha256',uniqid() /* random_bytes(10) */);
        $_SESSION['token'] = $token;
    } else {
      $token = $_SESSION['token'];
    }
    echo '<input type="hidden" name="token" value="' . $token . '">';
  }

  function get_token() {
      return (isset($_POST['token']) ? ($_POST['token']) : '');
  }

  function check_token($postToken) {
   //CSRF checks Token: if (check_token(get_token())) {
    if (!isset($_SESSION['token'])) {
        return false;
    }
    $token = $_SESSION['token'];
    if(strtolower($token) == strtolower($postToken)) {
        $_SESSION['token'] = '';
        unset($_SESSION['token']);
        return true;
    }
  }

  if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
    $name = $_POST['name'];
    $mail = $_POST['mail'];
    $discord = $_POST['discord'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    $phone = $_POST['phone'];
    $to = "admin@gameservers.co.il";
    $footer = "\n\n\nדואר אלקטרוני: " . $mail . "\r\n" .
                "שם מלא: " . $name . " | מספר פלאפון: " . $phone. " | דיסקורד: " . $discord;
    $succeed = false;
    $error = false;
	
    if(check_token(get_token()) == true) {
      if(!empty($name) && !empty($mail) && !empty($discord) && !empty($subject) && !empty($message)){
        sendMail($to, $subject . " - שם השולח: " . $name, $message . $footer);
        $succeed = true;
        $error = false;
      } else {
        $succeed = false;
        $error = false;
      }
    } else {
      $error = true;
    }

}

?>
	<div class="headerweb">
	</div>
	<section class="contact">
		<div class="container">
		<h2>צור קשר</h2>
			<div class="row">
			<div class="col-xs-6">
			<form method="post">
			 <?php
              if(isset($succeed) && $succeed == true)
              {
				echo "<span style='color: #1fc1f5;'><b>ההודעה נשלחה בהצלחה!</b></span><br>";
              }  else if(isset($succeed) && $succeed == false){
                echo "<span style='color: black;'>ההודעה לא נשלחה, אנא נסה שנית!</span><br>";
              }
              if(isset($error) && $error == true){
              }
            ?>
			<input type="text" name="name" placeholder="שם מלא" required><br>
			<input type="email" name="mail" placeholder="אימייל" required><br>
			<input type="text" name="discord" placeholder="דיסקורד"><br>
			<input type="text" name="phone" placeholder="טלפון"><br>
			<input type="text" name="subject" placeholder="נושא ההודעה" required><br>
			<textarea name="message" rows="8" placeholder="תוכן ההודעה" required=""></textarea><br>
			<?php echo csrf_field(); ?>
			<input type="submit" name="submit" value="שלח">
			</form>
			</div>
			<div class="col-xs-6">
				<a class="contact-icon discord" href="https://discordapp.com/invite/makNfrv" target="_blank"><i class="fab fa-discord"></i></a>
				<a class="contact-icon steam" href="#" onclick="clickSteam(event);"><i class="fab fa-steam-square"></i></a>
				<a class="contact-icon email" href="https://api.whatsapp.com/send?phone=972542069020" target="_blank"><i class="fab fa-whatsapp-square"></i></a>
				<br>
				<div id="contactsDiv">
				</div>
				<script type="text/javascript">
					function clickSteam(event) {
						event.preventDefault();
						$("#contactsDiv").html('\
							<a href="https://steamcommunity.com/id/zomby1/" target="_blank"> \
								<img src="https://badges.steamprofile.com/profile/default/steam/76561198065664801.png"> \
							</a> \
							<a  href="https://steamcommunity.com/id/zomby1/" target="_blank"> \
								<img src="https://badges.steamprofile.com/profile/default/steam/76561198065664801.png"> \
							</a> \
							<a href="https://steamcommunity.com/id/zomby1/" target="_blank" target="_blank"> \
								<img src="https://badges.steamprofile.com/profile/default/steam/76561198065664801.png"> \
							</a> \
						');
					}
					function clickEmail(event) {
						event.preventDefault();
						$("#contactsDiv").html('\
							admin@gameservers.co.il<br> \
							<br> \
							<br> \
						');
					}
				</script>
			</div>
			</div>
		</div>
	</section>
<?php
	function sendMail($recipients, $subject, $body, $altBody = null)
	{
		// Instantiation and passing `true` enables exceptions
		$mail = new PHPMailer(true);
		try
		{
			//Server settings
			//$mail->SMTPDebug = 4;                                       // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host       = 'smtp.gmail.com';					    // Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'il.gameservers@gmail.com';                     // SMTP username
			$mail->Password   = 'crodrrocgkgtejyz';                               // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;                                    // TCP port to connect to
			$mail->CharSet    = 'UTF-8';
			
			//Recipients
			$mail->setFrom('il.gameservers@gmail.com', 'GameServers.co.il Contact');
			if(is_array($recipients))
			{
				foreach($recipients as $recipient)
				{
					$mail->addAddress($recipient);               // Name is optional
				}
			}
			else
			{
				$mail->addAddress($recipients);
			}
			
			// Content
			$mail->Subject = $subject;
			$mail->Body    = $body;
			if($altBody === null)
			{
				$mail->isHTML(false);                                  // Set email format to HTML
			}
			else
			{
				$mail->isHTML(true);
				$mail->AltBody = $altBody;
			}
			
			$mail->send();
			
			return true;
		}
		catch (Exception $e)
		{
			return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
	}
?>