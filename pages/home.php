	<div class="headerweb" style="background: none;">
		<video autoplay muted loop class="videoBg">
		  <source src="assets/videos/csgo.mp4" type="video/mp4">
		</video>
		<div class="videoBgOverlay">
		</div>

		<div class="container">
			<h2><span class="boldgr">GameServers</span> <br/> שרתי משחק כחול לבן</h2>
			<h4><span class="welcometext">ברוכים הבאים אל </span><span class="boldgr2">GameServers</span> <br /> שרתי משחק כחול לבן
			<br /> חברתנו הוקמה בשנת 2019.
			<br /> במטרה להחזיר את הקהילה הישראלית לימיה הטובים
			<br /> איכות ושירות ללא פשרות
			<br /> אנו מבטיחים לתת את המחירים הכי נמוכים שאפשר
			<br /> אז למה אתם מחכים ?</h4>
			<div class="vis">
				<div class="row">
					<div class="col-md-4">
						<h5><span class="colorvi"><i class="fas fa-hdd"></i> </span>כונן SSD </h5>
						<img src="assets/images/headerserv.png" alt />
						<p>כל שירותנו רצים על כונן SDD <br />
						בכדי להבטיח מהירות מיטבית.</p>
					</div>
					<div class="col-md-4">
						<h5><span class="colorvi"><i class="fas fa-shield-alt"></i> </span>הגנות DDOS</h5>
						<img src="assets/images/headerserv.png" alt />
						<p>כל השרתים מוגנים 100% DDOS
						<br /> כדי להבטיח לכם הגנה מוחלטת</p>
					</div>
					<div class="col-md-4">
						<h5><span class="colorvi"><i class="fas fa-headset"></i> </span>תמיכה טכנית</h5>
						<img src="assets/images/headerserv.png" alt />
						<p>מענה ותמיכה במגוון פלטפורמות
							<br />דיסקורד, פייסבוק, סטים , אימייל</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h5><span class="colorvi"><i class="fas fa-comments"></i> </span>Live Chat</h5>
						<img src="assets/images/headerserv.png" alt />
						<p>שירות לייב צאט 
						<br /> מענה אנושי בלבד</p>
					</div>
					<div class="col-md-4">
						<h5><span class="colorvi"><i class="fas fa-upload"></i> </span>התקנה פשוטה</h5>
						<img src="assets/images/headerserv.png" alt />
						<p>התקנה קלה ומהירה של כל השירותים</p>
					</div>
					<div class="col-md-4">
						<h5><span class="colorvi"><i class="fas fa-cogs"></i> </span>פאנל ניהול ייחודי</h5>
						<img src="assets/images/headerserv.png" alt />
						<p>בנינו עבורכם פאנל ניהול חדשני
						<br /> עם מגוון אפשרויות שתוכלו לשלוט באופן מוחלט.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h5><span class="colorvi"><i class="fas fa-globe-europe"></i> </span>מיקום</h5>
						<img src="assets/images/headerserv.png" alt />
						<p>מיקום שרתינו
						<br /> במדינת ישראל בלבד</p>
					</div>
					<div class="col-md-4">
						<h5><span class="colorvi"><i class="fas fa-credit-card"></i> </span>רכישה אוטומטית</h5>
						<img src="assets/images/headerserv.png" alt />
						<p>מערכת רכישה אוטומטית
						<br /> מהיום תוכלו לקנות תמוצר ולקבל אותו מייד
						</p>
					</div>
					<div class="col-md-4">
					</div>
				</div>
			</div>
		</div>
	</div>
    <section class="service">
        <div class="container">
            <h2>
				עוד סיבה לבחור בנו<span>!</span><br>
פאנל ניהול חדשני 
שליטה מוחלטת בשרת משחק שלך
				<br>
<img src="https://i.imgur.com/qzQyCnk.png" width="100%" height="70%">
				
			</h2>
        </div>
    </section>
	<section class="servers">
	<div class="container">
	<h2>שרתים לדוגמה</h2>
	<div id="slider-two" class="owl-carousel owl-theme">
	<div class="item">
	<div class="servera">
		<img src="assets/images/cs166.png" class="imageser" alt \>
		<img src="assets/images/mafrid.png" class="imageser" alt \>
			<span class="ip">146.185.219.9:12345</span>
	</div>
	</div>
	<div class="item">
	<div class="servera">
		<img src="assets/images/csgoip.png" class="imageser" alt \>
		<img src="assets/images/mafrid.png" class="imageser" alt \>
			<span class="ip">146.185.219.9:27415</span>
	</div>
	</div>
	<div class="item">
	<div class="servera">
		<img src="assets/images/cs166.png" class="imageser" alt \>
		<img src="assets/images/mafrid.png" class="imageser" alt \>
			<span class="ip">146.185.219.9:12345</span>
	</div>
	</div>
	<div class="item">
	<div class="servera">
		<img src="assets/images/csgoip.png" class="imageser" alt \>
		<img src="assets/images/mafrid.png" class="imageser" alt \>
			<span class="ip">146.185.219.9:27415</span>
	</div>
	</div>
	<div class="item">
	<div class="servera">
		<img src="assets/images/cs166.png" class="imageser" alt \>
		<img src="assets/images/mafrid.png" class="imageser" alt \>
			<span class="ip">146.185.219.9:12345</span>
	</div>
	</div>
	<div class="item">
	<div class="servera">
		<img src="assets/images/csgoip.png" class="imageser" alt \>
		<img src="assets/images/mafrid.png" class="imageser" alt \>
			<span class="ip">146.185.219.9:27415</span>
	</div>
	</div>
	</div>
	</div>
	</section>
	<section class="plans">
		<div class="container">
		<h2>חבילות מומלצות</h2>
			<div id="slider-plans" class="owl-carousel owl-theme">
			<div class="item">
			<div class="plan">
			<div class="plan-header">
			</div>
			<p>שחקנים: 18</p>
			<div class="afterp"></div>
			<p>תוקף השרת: 30 ימים</p>
			<div class="afterp"></div>
			<p>פאנל ניהול: OPG</p>
			<div class="afterp"></div>
			<p>מיקום: ישראל</p>
			<div class="afterp"></div>
			<p>תמיכה: 24/7</p>
			<div class="afterp"></div>
			<p>הגנות DDoS מתקדמות</p>
			<div class="afterp"></div>
			<p>מחיר: 18 ש"ח</p>
			<div class="afterp"></div>
			<a href="https://hosting1.site/?m=simple-billing&p=shop_guest" target="_blank"><img src="assets/images/pbutton.png" class="buttonp" alt \></a>
			</div>
			</div>
			<div class="item">
			<div class="plan">
			<div class="plan-header-csgo">
			</div>
			<p>שחקנים: 24</p>
			<div class="afterp"></div>
			<p>תוקף השרת: 30 ימים</p>
			<div class="afterp"></div>
			<p>פאנל ניהול: OPG</p>
			<div class="afterp"></div>
			<p>מיקום: ישראל</p>
			<div class="afterp"></div>
			<p>תמיכה: 24/7</p>
			<div class="afterp"></div>
			<p>הגנות DDoS מתקדמות</p>
			<div class="afterp"></div>
			<p>מחיר: 48 ש"ח</p>
			<div class="afterp"></div>
			<a href="https://hosting1.site/?m=simple-billing&p=shop_guest" target="_blank"><img src="assets/images/pbutton.png" class="buttonp" alt \></a>
			</div>
			</div>
			<div class="item">
			<div class="plan">
			<div class="plan-header">
			</div>
			<p>שחקנים: 28</p>
			<div class="afterp"></div>
			<p>תוקף השרת: 30 ימים</p>
			<div class="afterp"></div>
			<p>פאנל ניהול: OPG</p>
			<div class="afterp"></div>
			<p>מיקום: ישראל</p>
			<div class="afterp"></div>
			<p>תמיכה: 24/7</p>
			<div class="afterp"></div>
			<p>הגנות DDoS מתקדמות</p>
			<div class="afterp"></div>
			<p>מחיר: 28 ש"ח</p>
			<div class="afterp"></div>
			<a href="https://hosting1.site/?m=simple-billing&p=shop_guest" target="_blank"><img src="assets/images/pbutton.png" class="buttonp" alt \></a>
			</div>
			</div>
			<div class="item">
			<div class="plan">
			<div class="plan-header-csgo">
			</div>
			<p>שחקנים: 32</p>
			<div class="afterp"></div>
			<p>תוקף השרת: 30 ימים</p>
			<div class="afterp"></div>
			<p>פאנל ניהול: OPG</p>
			<div class="afterp"></div>
			<p>מיקום: ישראל</p>
			<div class="afterp"></div>
			<p>תמיכה: 24/7</p>
			<div class="afterp"></div>
			<p>הגנות DDoS מתקדמות</p>
			<div class="afterp"></div>
			<p>מחיר: 64 ש"ח</p>
			<div class="afterp"></div>
			<a href="https://hosting1.site/?m=simple-billing&p=shop_guest" target="_blank"><img src="assets/images/pbutton.png" class="buttonp" alt \></a>
			</div>
			</div>
			</div>
		</div>
	</section>