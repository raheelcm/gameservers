<?php
?>
    <div class="headerweb">
    </div>
    <section class="contact">
        <div class="container">
            <h2>אדמין פאנל</h2>
            <div class="row">
                <div class="col-xs-12">
                    <div id="admin_menu">
                        <a href="/admin_tickets" class="bordered">כרטיסי תמיכה</a>
                        <a href="/manage_packages" class="bordered">ניהול חבילות</a>
						<a href="/add_package" class="bordered">הוסף חבילה</a>
                    </div>
                    <table class="table table-striped">
						  <thead>
							<tr>
							  <th scope="col">#</th>
							  <th scope="col">שם</th>
							  <th scope="col">משחק</th>
							  <th scope="col">מחיר</th>
							  <th scope="col">פעולה</th>
							</tr>
						  </thead>
						  <tbody>
							<tr>
							  <th scope="row">1</th>
							  <td>חבילה משתלמת</td>
							  <td>Counter Strike 1.6</td>
							  <td>50</td>
							  <td><a href="/#edit" id="edit_pack">עריכה</a> // <a href="/#delete" id="delete_pack">מחיקה</a></td>
							</tr>
							<tr>
							  <th scope="row">1</th>
							  <td>חבילה משתלמת</td>
							  <td>Counter Strike 1.6</td>
							  <td>50</td>
							  <td><a href="/#edit" id="edit_pack">עריכה</a> // <a href="/#delete" id="delete_pack">מחיקה</a></td>
							</tr>
							<tr>
							  <th scope="row">1</th>
							  <td>חבילה משתלמת</td>
							  <td>Counter Strike 1.6</td>
							  <td>50</td>
							  <td><a href="/#edit" id="edit_pack">עריכה</a> // <a href="/#delete" id="delete_pack">מחיקה</a></td>
							</tr>
						  </tbody>
						</table>
                </div>
            </div>
        </div>
    </section>