<?php
?>
    <div class="headerweb">
    </div>
    <section class="contact">
        <div class="container">
            <h2>פאנל משתמש</h2>
            <div class="row">
                <div class="col-xs-12">
                    <div id="admin_menu">
                        <a href="/user_tickets" class="bordered">כרטיסי תמיכה</a>
                        <a href="/my_orders" class="bordered">ההזמנות שלי</a>
                    </div>
                   <table class="table table-striped">
						  <thead>
							<tr>
							  <th scope="col">#</th>
							  <th scope="col">שם</th>
							  <th scope="col">משחק</th>
							  <th scope="col">מחיר</th>
							  <th scope="col">תאריך</th>
							</tr>
						  </thead>
						  <tbody>
							<tr>
							  <th scope="row">1</th>
							  <td>חבילה משתלמת</td>
							  <td>Counter Strike 1.6</td>
							  <td>50</td>
							  <td>10/06/2019</td>
							</tr>
							<tr>
							  <th scope="row">1</th>
							  <td>חבילה משתלמת</td>
							  <td>Counter Strike 1.6</td>
							  <td>50</td>
							  <td>10/06/2019</td>
							</tr>
							<tr>
							  <th scope="row">1</th>
							  <td>חבילה משתלמת</td>
							  <td>Counter Strike 1.6</td>
							  <td>50</td>
							  <td>10/06/2019</td>
							</tr>
						  </tbody>
						</table>
                
				</div>
            </div>
        </div>
    </section>