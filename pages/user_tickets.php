<?php
?>
    <div class="headerweb">
    </div>
    <section class="contact">
        <div class="container">
            <h2>פאנל משתמש</h2>
            <div class="row">
                <div class="col-xs-12">
                    <div id="admin_menu">
                        <a href="/user_tickets" class="bordered">כרטיסי תמיכה</a>
                        <a href="/my_orders" class="bordered">ההזמנות שלי</a>
                    </div>
                    <table class="table table-striped">
						  <thead>
							<tr>
							  <th scope="col">#</th>
							  <th scope="col">שם</th>
							  <th scope="col">מצב</th>
							  <th scope="col">פעולה</th>
							</tr>
						  </thead>
						  <tbody>
							<tr>
							  <th scope="row">1</th>
							  <td>כרטיס תמיכה 1</td>
							  <td>פתוח</td>
							  <td><a href="/user_tickets?id=1" id="show_details">הצג פרטים</a></td>
							</tr>
							<tr>
							  <th scope="row">2</th>
							  <td>כרטיס תמיכה 2</td>
							  <td>פתוח</td>
							  <td><a href="/user_tickets?id=1" id="show_details">הצג פרטים</a></td>
							</tr>
							<tr>
							  <th scope="row">3</th>
							  <td>כרטיס תמיכה 3</td>
							  <td>פתוח</td>
							  <td><a href="/user_tickets?id=1" id="show_details">הצג פרטים</a></td>
							</tr>
						  </tbody>
						</table>
                </div>
            </div>
        </div>
    </section>